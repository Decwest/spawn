import h5py
import cv2
import numpy as np
import os
from tqdm import tqdm
import sys

from utils import nyu
from utils import path_config

# Required path setup
path_config = path_config.read_config()

NYU_V2_PATH = path_config["NYU_RAW"]
matlab_file = os.path.join(NYU_V2_PATH,'nyu_depth_v2_labeled.mat')

MAX_DEPTH  =  10.0

print("Reading NYU Matlab file from ", matlab_file, "...", sep="")
f = h5py.File(matlab_file, 'r')


images = f['images']
depths = f['depths']
labels = np.array(f['labels'])

class_colors = np.array([
    [0, 0, 0],
    [0, 1, 0],
    [0, .7, 0],
    [0.66, 0.66, 1],
    [0.8, 0.8, 1],
    [1, 1, 0.0392201],
    [1, 0.490452, 0.0624932],
    [0.657877, 0.0505005, 1],
    [0.0363214, 0.0959549, 0.6],
    [0.316852, 0.548847, 0.186899],
    [.8, .5, .1],
    [1, 0.241096, 0.718126],
  ])

class_colors_bgr = np.flip(class_colors,1)

map894_11 = np.array(nyu.get_894_11_class_mapping())

print("Processing labels...")
for n, (image_data, depth_data, label_data) in tqdm(enumerate(zip(images, depths, labels)),total=len(labels), file=sys.stdout):

    # color image
    img_ = np.empty([480, 640, 3])
    img_[:,:,0] = image_data[0,:,:].T
    img_[:,:,1] = image_data[1,:,:].T
    img_[:,:,2] = image_data[2,:,:].T
    
    # depth image
    depth_ = np.empty([480, 640, 3])
    depth_[:,:,0] = depth_data[:,:].T / MAX_DEPTH * 255
    depth_[:,:,1] = depth_data[:,:].T / MAX_DEPTH * 255
    depth_[:,:,2] = depth_data[:,:].T / MAX_DEPTH * 255
    

    # label image
    label_image = class_colors_bgr[map894_11[label_data.T]] * 255

    file_name = 'NYU{:04d}'.format(n+1) + '_0000'

    if os.path.isfile(os.path.join(NYU_V2_PATH,'NYUtest',file_name+'.bin')):
        # color image
        cv2.imwrite(os.path.join(NYU_V2_PATH,'NYUtest',file_name+'_color.jpg'), img_)
        # depth image
        cv2.imwrite(os.path.join(NYU_V2_PATH,'NYUtest',file_name+'_depth.png'), depth_)
        # label image
        cv2.imwrite(os.path.join(NYU_V2_PATH,'NYUtest',file_name+'_labels_rgb.png'), label_image)
        cv2.imwrite(os.path.join(NYU_V2_PATH,'NYUtest',file_name+'_labels.png'), map894_11[label_data.T])
    elif os.path.isfile(os.path.join(NYU_V2_PATH,'NYUtrain',file_name+'.bin')):
        # color image
        cv2.imwrite(os.path.join(NYU_V2_PATH,'NYUtrain',file_name+'_color.jpg'), img_)
        # depth image
        cv2.imwrite(os.path.join(NYU_V2_PATH,'NYUtrain',file_name+'_depth.png'), depth_)
        
        # label image
        cv2.imwrite(os.path.join(NYU_V2_PATH,'NYUtrain',file_name+'_labels_rgb.png'), label_image)
        cv2.imwrite(os.path.join(NYU_V2_PATH,'NYUtrain',file_name+'_labels.png'), map894_11[label_data.T])
    else:
        print("Warning: NYU 3D labels not found: ", file_name+".bin")
        print("Have you downloaded it? Check your path.conf file.")
        break
