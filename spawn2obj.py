import argparse
from utils import path_config
import os

# default settings
GPU = 0


# Dataloader settings
BATCH_SIZE = 1
WORKERS = 2
DATASET = "NYU"
PREPROC_PATH = ""
VOL_PATH = ""

# Model settings
WEIGHTS = "none"
BATCH_NORM = True
INSTANCE_NORM = False
INPUT_TYPE = "depth+rgb+normals"
FINE_TUNE = False
ORACLE=False
SCENE=""
FILE=""

SSCNET = False

def parse_arguments():
    global GPU, \
           BATCH_SIZE, WORKERS, DATASET, PREPROC_PATH, VOL_PATH, SSCNET, \
           WEIGHTS, BATCH_NORM, INSTANCE_NORM, INPUT_TYPE, FINE_TUNE, ORACLE, SCENE, FILE

    print("\nMMNet Image Generator Script\n")

    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", help="Target dataset", type=str, choices=['SUNCG', 'NYU', 'NYUCAD'])
    parser.add_argument("train_valid", help="Target set", type=str, choices=['train', 'valid'])
    parser.add_argument("scene", help="Target scene", type=str)
    parser.add_argument("weights", help="Pretraind weights. ", type=str)
    parser.add_argument("--workers", help="Concurrent threads. Default " + str(WORKERS),
                        type=int, default=WORKERS, required=False)
    parser.add_argument("--gpu", help="GPU device. Default " + str(GPU),
                        type=int, default=GPU, required=False)
    parser.add_argument("--input_type",  help="Network input type. Default " + INPUT_TYPE,
                        type=str, default=INPUT_TYPE, required=False,
                        choices=['depth+rgb+normals', 'depth+rgb', 'depth']
                        )
    parser.add_argument("--bn",  help="Apply batch bormalization?. Default yes",
                        type=str, default="yes", required=False,
                        choices=['yes', 'Yes', 'y', 'Y', 'no', 'No', 'n', 'N']
                        )
    parser.add_argument("--fine_tune",  help="Is it a fine tune from a suncg model?. Default no",
                        type=str, default="no", required=False,
                        choices=['yes', 'Yes', 'y', 'Y', 'no', 'No', 'n', 'N']
                        )
    parser.add_argument("--oracle",  help="Is it an oracle test?. Default no",
                        type=str, default="no", required=False,
                        choices=['yes', 'Yes', 'y', 'Y', 'no', 'No', 'n', 'N']
                        )

    parser.add_argument("--sscnet",  help="SSCNet Baseline? Default no",
                        type=str, default="no", required=False,
                        choices=['yes', 'Yes', 'y', 'Y', 'no', 'No', 'n', 'N']
                        )


    args = parser.parse_args()

    SCENE = args.scene
    train_valid = args.train_valid

    DATASET = args.dataset
    WORKERS = args.workers
    GPU = args.gpu
    WEIGHTS = args.weights
    INPUT_TYPE = args.input_type
    BATCH_NORM = args.bn in ['yes', 'Yes', 'y', 'Y']
    FINE_TUNE = args.fine_tune in ['yes', 'Yes', 'y', 'Y']
    ORACLE = args.oracle in ['yes', 'Yes', 'y', 'Y']
    SSCNET = args.sscnet in ['yes', 'Yes', 'y', 'Y']


    path_dict = path_config.read_config()

    if DATASET == "NYU":
        VOL_PATH = path_dict["NYU_VOL_PATH"]
        if not FINE_TUNE:
            if INPUT_TYPE == "depth+rgb":
                PREPROC_PATH = path_dict["NYU_RGB_PRIOR_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYU_RGB_NORMALS_PRIOR_PREPROC"]
        else:
            if INPUT_TYPE == "depth+rgb":
                PREPROC_PATH = path_dict["NYU_RGB_PRIOR_FINE_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYU_RGB_NORMALS_FINE_PRIOR_PREPROC"]
        FILE = os.path.join(PREPROC_PATH, train_valid, SCENE)

    elif DATASET == "NYUCAD":
        VOL_PATH = path_dict["NYUCAD_VOL_PATH"]
        if not FINE_TUNE:
            if INPUT_TYPE == "depth+rgb":
                PREPROC_PATH = path_dict["NYUCAD_RGB_PRIOR_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYUCAD_RGB_NORMALS_PRIOR_PREPROC"]
        else:
            if INPUT_TYPE == "depth+rgb":
                PREPROC_PATH = path_dict["NYUCAD_RGB_PRIOR_FINE_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYUCAD_RGB_NORMALS_FINE_PRIOR_PREPROC"]
        FILE = os.path.join(PREPROC_PATH, train_valid, SCENE)

    elif DATASET == "SUNCG":
        VOL_PATH = path_dict["SUNCG_VOL_PATH"]
        if not FINE_TUNE:
            if INPUT_TYPE == "depth+rgb":
                PREPROC_PATH = path_dict["SUNCG_RGB_PRIOR_PREPROC"]
            else:
                PREPROC_PATH = path_dict["SUNCG_RGB_NORMALS_PRIOR_PREPROC"]
            FILE = os.path.join(PREPROC_PATH, train_valid, SCENE[:2], SCENE )
        else:
            print("Fine-tuning from SUNCG is only possible with NYU or NYUCAD Datasets")
            exit(-1)
    else:
        print("Dataset", DATASET, "not supported yet!")
        exit(-1)


def torch2np(t,shape):
    return t.detach().cpu().numpy().reshape(shape)


def augment(v1, i):
    import numpy as np

    o1 = v1.copy()

    if i & 0b001:
        o1 = np.flip(o1, axis=0)
    if i & 0b010:
        o1 = np.flip(o1, axis=2)
    if i & 0b100:
        o1 = np.swapaxes(o1, axis1=0, axis2=2)

    return o1




def generate_obj():
    from tqdm import tqdm
    import os

    from utils.data import SSCMultimodalDataset, sample2dev
    from utils.data import get_file_prefixes_from_path
    from utils.obj import vox2obj, vol2obj, weights2obj
    from torch.utils.data import DataLoader
    import torch
    from utils.cuda import get_device
    import numpy as np
    from utils.metrics import MIoU, CompletionIoU
    from models.spawn import get_mmnet
    # from models.sscnet import get_sscnet

    import h5py
    import glob

    nyu_classes = ["ceil", "floor", "wall", "window", "chair", "bed", "sofa", "table", "tvs", "furniture", "objects",
                   "empty"]

    print("Selected device:", "cuda:" + str(GPU))

    dev = get_device("cuda:" + str(GPU))
    torch.cuda.empty_cache()

    valid_prefixes = [FILE]

    print(FILE, valid_prefixes)

    valid_ds = SSCMultimodalDataset(valid_prefixes)

    dataloader = DataLoader(valid_ds, batch_size=BATCH_SIZE, shuffle=False, num_workers=1)

    vox_shape = (240, 144, 240)
    vox_shape_down = (60, 36, 60)
    batch_shape = (1, 60, 36, 60)
    batch_shape_ch = (1, 60, 36, 60, 12)
    obj_dir = "obj"


    if not SSCNET:
        model = get_mmnet(input_type=INPUT_TYPE, batch_norm=BATCH_NORM, inst_norm=False)
    else:
        model = get_sscnet()


    print("loading", WEIGHTS)
    model.load_state_dict(torch.load(os.path.join("weights", WEIGHTS)))

    model.to(dev)
    model.eval()

    torch.cuda.empty_cache()

    for sample, prefix in zip(dataloader, valid_prefixes):
        sample = sample2dev(sample, dev)

        vox_tsdf = sample['vox_tsdf']
        vox_grid = (abs(torch2np(vox_tsdf,vox_shape)) == 1.0).astype(np.uint8)
        gt = sample['gt']
        if not ORACLE:
            vox_prior = sample['vox_prior']
            vox_prior_orig = vox_prior.clone()

        else:
            _, surf = torch.max(sample['vox_prior'], dim=1)
            no_surf_idx = surf == 0
            oracle_gt = gt.clone()
            oracle_gt[no_surf_idx] = 0


            vox_prior = torch.nn.functional.one_hot(oracle_gt.to(torch.int64), num_classes=12).to(
                torch.float).transpose(3, 4).transpose(2, 3).transpose(1, 2)
        vox_weights = sample['vox_weights']

        basename = os.path.basename(prefix)

        """
        if DATASET != "SUNCG":
            matfile = os.path.join(VOL_PATH, basename + '_vol_d4.mat')
        else:
            search_criteria =  os.path.join(VOL_PATH, '*' + basename[:46] + '*_vol_d4.mat')
            result = glob.glob(search_criteria)
            if len(result) == 1:
                matfile = result[0]
            else:
                print("Unexpected list of matching mat files:")
                print(result)
                exit(-1)

        # Evaluation volume as defined by SSCNET Song et. al. 2017
        f = h5py.File(matfile, 'r')
        vol = np.array(f['flipVol_ds'])
        vol = torch.tensor((vol)).to(dev)
        vol = vol.view(list(vox_weights.size()))
        """
        #vox_weights[vol < -1] = 0  # Outside room as defined by SSCNET Song et. al. 2017

        pred = model(vox_tsdf, vox_prior)

        vox_prior = torch2np(torch.max(vox_prior, dim=1)[1], vox_shape_down)
        pred = torch2np(torch.max(pred, dim=1)[1],vox_shape_down)
        gt = torch2np(gt, vox_shape_down)
        vox_weights = torch2np(vox_weights, vox_shape_down)

        #vol = torch2np(vol, vox_shape_down)

        #idx = (np.abs(vol) >= 1) & (vol != -1.0)

        if SSCNET:
            basename = DATASET+"_SSCNet_"+os.path.basename(FILE)
        else:
            if not FINE_TUNE:
                basename = DATASET+"_"+INPUT_TYPE+"_"+os.path.basename(FILE)
            else:
                basename = DATASET+"_"+INPUT_TYPE+"_FINE_"+os.path.basename(FILE)

        pred[vox_weights==0]=0
        #gt[vox_weights==0]=0
        #vox_prior[vox_weights==0]=0

        #pred[idx] = 0
        #gt[idx] = 0
        #vox_prior[idx]=0

        print("GT...")
        gt_file = os.path.join(obj_dir, basename) + "_gt"
        vox2obj(gt_file, gt, vox_shape_down, 0.08, include_top=True, triangular=False, inner_faces=True,
                complete_walls=False, colors=None, gap=0.08)

        print("Predicted volume...")
        pred_file = os.path.join(obj_dir, basename) + "_pred"
        vox2obj(pred_file, pred, vox_shape_down, 0.08, include_top=True, triangular=False,
                inner_faces=True,
                complete_walls=False, colors=None, gap=0.08)

        if not SSCNET:
            print("Surface voxels...")
            surface_file = os.path.join(obj_dir, basename) + "_surf"
            vox2obj(surface_file, vox_grid, vox_shape, 0.02, include_top=True, triangular=False, inner_faces=True,
                    complete_walls=False, colors=None, gap=0.08)



            """
            # Augmented scenes     
    
            for i in range(8):
                print("Augmenation",i)
                gt_aug = augment(gt, i)
                gt_file = os.path.join(obj_dir, basename) + "_gt" + str(i)
                print(gt_file)
                vox2obj(gt_file, gt_aug, vox_shape_down, 0.08, include_top=True, triangular=False, inner_faces=True,
                        complete_walls=False, colors=None, gap=0.08)
            """

            print("Surface Prior volume...")
            prior_file = os.path.join(obj_dir, basename) + "_prior"
            vox2obj(prior_file, vox_prior, vox_shape_down, 0.08, include_top=True, triangular=False,
                    inner_faces=True,
                    complete_walls=False, colors=None, gap=0.08)


            #print("Weights volume...")
            #weights_file = os.path.join(obj_dir, basename) + "_weights"
            #weights2obj(weights_file, vox_weights, vox_shape_down, 0.08, gap=0.08)

            #print("Vol volume...")
            #vol_file = os.path.join(obj_dir, basename) + "_vol"
            #vol2obj(vol_file, vol, vox_shape_down, 0.08, gap=0.08)

    print("Done! You can open the generated volumes with Blender!\n")




# Main Function
def main():
    parse_arguments()
    generate_obj()


if __name__ == '__main__':
    main()

